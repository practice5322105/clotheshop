package com.example.clotheshop.controller;

import com.example.clotheshop.entity.Order;
import com.example.clotheshop.entity.Product;
import com.example.clotheshop.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @GetMapping
    public List<Order> allOrders() {
        return orderService.getAll();
    }

    @GetMapping("/{id}")
    public Order getById(@PathVariable Long id) {
        return orderService.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(@PathVariable Long id) {
        orderService.deleteById(id);
    }

    @PostMapping("/{id}/cancel")
    public Boolean cancelOrder(@PathVariable Long id) {
        return orderService.cancelOrder(id);
    }

    @PostMapping("/{id}/purchase")
    public Order purchaseOrder(@PathVariable Long id) {
        return orderService.purchaseOrder(id);
    }

    @GetMapping("/{id}/items")
    public List<Product> getProductsOrder(@PathVariable Long id) {
        return orderService.getProductsOfOrder(id);
    }

    @PostMapping("/{id}/items")
    public Order addItemOrder(@PathVariable Long id, Order order) {
        return orderService.addItemToOrder(id, order);
    }

    @PostMapping("/{oid}/items/pid")
    public Product getItemOrder(@PathVariable Long oid, @PathVariable Long pid) {
        return orderService.getItemOfOrder(oid, pid);
    }


    @DeleteMapping("/{oid}/items/pid")
    public void deleteItemOrder(@PathVariable Long oid, @PathVariable Long pid) {
        orderService.deleteItemOfOrder(oid, pid);
    }


}
