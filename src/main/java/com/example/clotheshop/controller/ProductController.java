package com.example.clotheshop.controller;

import com.example.clotheshop.delegate.ProductDelegate;
import com.example.clotheshop.dto.ProductDto;
import com.example.clotheshop.entity.Product;
import com.example.clotheshop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductDelegate productDelegate;


    @GetMapping
    public List<ProductDto> getAllProductDtos() {
        return productDelegate.getAll();
    }

    @GetMapping("/{id}")
    public ProductDto getById(@PathVariable Long id) {
        return productDelegate.getById(id);
    }

    @PostMapping
    public ProductDto getById(@RequestBody ProductDto product) {
        return productDelegate.save(product);
    }

    @PutMapping("/{id}")
    public ProductDto getById(@PathVariable Long id, @RequestBody ProductDto product) {
        return productDelegate.update(id, product);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        productDelegate.deleteById(id);
    }


}
