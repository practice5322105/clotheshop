package com.example.clotheshop.controller;

import com.example.clotheshop.delegate.CategoryDelegate;
import com.example.clotheshop.dto.CategoryDto;
import com.example.clotheshop.dto.ProductDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryDelegate categoryDelegate;


    @GetMapping
    public List<CategoryDto> productCategories() {
        return categoryDelegate.getAll();
    }

    @PostMapping
    public CategoryDto createCategory(CategoryDto category) {
        return categoryDelegate.save(category);
    }

    @GetMapping("/{id}")
    public CategoryDto individualCategory(@PathVariable Long id) {
        return categoryDelegate.getById(id);
    }

    @GetMapping("/{id}/products")
    public List<ProductDto> getProductsOfCategories(@PathVariable Long id) {
        return categoryDelegate.getProductsOfCategories(id);
    }

}
