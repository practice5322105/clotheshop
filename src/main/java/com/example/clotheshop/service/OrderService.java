package com.example.clotheshop.service;

import com.example.clotheshop.entity.Order;
import com.example.clotheshop.entity.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OrderService {
    List<Order> getAll();

    Order getById(Long id);

    void deleteById(Long id);

    Boolean cancelOrder(Long id);

    Order purchaseOrder(Long id);

    List<Product> getProductsOfOrder(Long id);

    Product getItemOfOrder(Long OrderId, Long ItemId);

    void deleteItemOfOrder(Long OrderId, Long ItemId);

    Order addItemToOrder(Long id, Order order);
}
