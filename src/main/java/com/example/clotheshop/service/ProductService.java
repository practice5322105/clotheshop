package com.example.clotheshop.service;

import com.example.clotheshop.entity.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    List<Product> getAll();

    Product getById(Long id);

    Product save(Product product);

    Product update(Long id, Product product);

    void deleteById(Long id);
}