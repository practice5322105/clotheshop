package com.example.clotheshop.service;

import com.example.clotheshop.entity.Category;
import com.example.clotheshop.entity.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoryService {
    List<Category> getAll();

    Category getById(Long id);

    Category save(Category category);

    List<Product> getProductsOfCategories(Long id);
}
