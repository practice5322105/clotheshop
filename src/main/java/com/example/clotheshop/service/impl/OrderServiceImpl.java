package com.example.clotheshop.service.impl;

import com.example.clotheshop.entity.Order;
import com.example.clotheshop.entity.Product;
import com.example.clotheshop.repository.OrderRepository;
import com.example.clotheshop.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;

    @Override
    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    @Override
    public Order getById(Long id) {
        return orderRepository.findById(id).get();
    }

    @Override
    public void deleteById(Long id) {
        orderRepository.deleteById(id);
    }

    @Override
    public Boolean cancelOrder(Long id) {
        return orderRepository.cancelOrder(id);
    }

    @Override
    public Order purchaseOrder(Long id) {
        return null;
    }

    @Override
    public List<Product> getProductsOfOrder(Long id) {
        return null;
    }

    @Override
    public Product getItemOfOrder(Long OrderId, Long ItemId) {
        return null;
    }

    @Override
    public void deleteItemOfOrder(Long OrderId, Long ItemId) {

    }

    @Override
    public Order addItemToOrder(Long id, Order order) {
        return null;
    }
}
