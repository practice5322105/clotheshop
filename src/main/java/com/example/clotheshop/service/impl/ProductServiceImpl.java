package com.example.clotheshop.service.impl;

import com.example.clotheshop.entity.Product;
import com.example.clotheshop.repository.ProductRepository;
import com.example.clotheshop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {


    private final ProductRepository productRepository;

    @Override
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @Override
    public Product getById(Long id) {
        return productRepository.getById(id);
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product update(Long id, Product product) {
        Product toBeUpdated = productRepository.getById(id);
        toBeUpdated.setCategory(product.getCategory());
        toBeUpdated.setName(product.getName());
        toBeUpdated.setPrice(product.getPrice());
        toBeUpdated.setQuantity(product.getQuantity());
        productRepository.save(toBeUpdated);
        return toBeUpdated;
    }

    @Override
    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }
}

