package com.example.clotheshop.service.impl;

import com.example.clotheshop.entity.Category;
import com.example.clotheshop.entity.Product;
import com.example.clotheshop.repository.CategoryRepository;
import com.example.clotheshop.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    @Override
    public List<Category> getAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category getById(Long id) {
        return categoryRepository.findById(id).get();
    }

    @Override
    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public List<Product> getProductsOfCategories(Long id) {
        Category category = categoryRepository.findCategoryById(id);
        return category.getProduct();
    }
}
