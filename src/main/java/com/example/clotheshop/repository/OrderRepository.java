package com.example.clotheshop.repository;

import com.example.clotheshop.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    @Query("update Order u set u.complete = FALSE  where u.id = ?1")
    Boolean cancelOrder(Long id);

}
