package com.example.clotheshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClotheshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClotheshopApplication.class, args);
	}

}
