package com.example.clotheshop.mapper;

import com.example.clotheshop.dto.CategoryDto;
import com.example.clotheshop.dto.ProductDto;
import com.example.clotheshop.entity.Category;
import com.example.clotheshop.entity.Product;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    Product toEntity(ProductDto productDto);

    ProductDto toDto(Product product);

    List<ProductDto> toListDto(List<Product> productList);
}
