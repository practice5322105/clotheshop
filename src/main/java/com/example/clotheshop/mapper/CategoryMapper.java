package com.example.clotheshop.mapper;

import com.example.clotheshop.dto.CategoryDto;
import com.example.clotheshop.entity.Category;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CategoryMapper {
    Category toEntity(CategoryDto categoryDto);

    CategoryDto toDto(Category category);

    List<CategoryDto> toListDto(List<Category> categoryList);
}
