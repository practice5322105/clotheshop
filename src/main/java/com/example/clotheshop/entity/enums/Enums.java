package com.example.clotheshop.entity.enums;

public enum Enums {
    PLACED,
    APPROVED,
    DELIVERED
}
