package com.example.clotheshop.entity;


import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;
@Data
@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate shipDate;
    private LocalDate createdAt;
    private String status;
    private Boolean complete;
    @OneToMany
    private List<Product> product;

}
