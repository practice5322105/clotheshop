package com.example.clotheshop.delegate;

import com.example.clotheshop.dto.CategoryDto;
import com.example.clotheshop.dto.ProductDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoryDelegate {
    List<CategoryDto> getAll();

    CategoryDto save(CategoryDto category);

    CategoryDto getById(Long id);

    List<ProductDto> getProductsOfCategories(Long id);
}
