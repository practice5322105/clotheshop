package com.example.clotheshop.delegate;

import com.example.clotheshop.dto.ProductDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductDelegate {
    List<ProductDto> getAll();

    ProductDto getById(Long id);

    ProductDto save(ProductDto product);

    ProductDto update(Long id, ProductDto product);

    void deleteById(Long id);
}
