package com.example.clotheshop.delegate.impl;

import com.example.clotheshop.delegate.CategoryDelegate;
import com.example.clotheshop.dto.CategoryDto;
import com.example.clotheshop.dto.ProductDto;
import com.example.clotheshop.entity.Category;
import com.example.clotheshop.mapper.CategoryMapper;
import com.example.clotheshop.mapper.ProductMapper;
import com.example.clotheshop.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryDelegateImpl implements CategoryDelegate {
    private final CategoryMapper categoryMapper;
    private final ProductMapper productMapper;
    private final CategoryService categoryService;

    @Override
    public List<CategoryDto> getAll() {
        return categoryMapper.toListDto(categoryService.getAll());
    }

    @Override
    public CategoryDto save(CategoryDto categoryDto) {
        Category category = categoryMapper.toEntity(categoryDto);
        categoryService.save(category);
        return categoryMapper.toDto(category);
    }

    @Override
    public CategoryDto getById(Long id) {
        return categoryMapper.toDto(categoryService.getById(id));
    }

    @Override
    public List<ProductDto> getProductsOfCategories(Long id) {
        return productMapper.toListDto(categoryService.getProductsOfCategories(id));
    }
}
