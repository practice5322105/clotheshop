package com.example.clotheshop.delegate.impl;

import com.example.clotheshop.delegate.ProductDelegate;
import com.example.clotheshop.dto.ProductDto;
import com.example.clotheshop.entity.Product;
import com.example.clotheshop.mapper.ProductMapper;
import com.example.clotheshop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductDelegateImpl implements ProductDelegate {
    private final ProductMapper productMapper;
    private final ProductService productService;

    @Override
    public List<ProductDto> getAll() {

        return productMapper.toListDto(productService.getAll());
    }

    @Override
    public ProductDto getById(Long id) {

        return productMapper.toDto(productService.getById(id));
    }

    @Override
    public ProductDto save(ProductDto productDto) {
        Product product = productMapper.toEntity(productDto);
        return productMapper.toDto(productService.save(product));
    }

    @Override
    public ProductDto update(Long id, ProductDto productDto) {
        Product product = productMapper.toEntity(productDto);

        return productMapper.toDto(productService.update(id, product));
    }

    @Override
    public void deleteById(Long id) {
        productService.deleteById(id);
    }
}
